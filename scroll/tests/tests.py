# -*- coding: utf-8 -*-

import os
import unittest
from reportlab.lib.pagesizes import landscape, portrait, A4
from pyPdf import PdfFileReader
from scroll.models import Scroll

TEST_PDF = 'test.pdf'

class ScrollTest(unittest.TestCase):

    def setUp(self):
        if os.path.exists(TEST_PDF):
            os.remove(TEST_PDF)

    def tearDown(self):
        if os.path.exists(TEST_PDF):
            os.remove(TEST_PDF)

    @property
    def pdf(self):
        return PdfFileReader(file(TEST_PDF))

    def test_pdf(self):
        scroll = Scroll(TEST_PDF)
        scroll.register_font('regular', 'ttf/Ubuntu-L.ttf')
        page1 = scroll.add_page('pdf/page1.pdf')
        page1.add_string('Blah blah blah')

        page1.add_string(x=100, y=100, text='Blah blah blah')


        page2 = scroll.add_page('pdf/page2.pdf')
        page2.add_SVG('svg/polygon_rect_path.svg', scale=2)

        scroll.render()
        assert os.path.isfile(TEST_PDF)
        page_count = self.pdf.numPages
        assert page_count is 2

    def test_portrait_pdf(self):
        size = portrait(A4)
        self.assertEqual(tuple(reversed(size)),
                         landscape(A4))
        scroll = Scroll(TEST_PDF, size)
        self.assertEqual(scroll.size, size)

        scroll.register_font('regular', 'ttf/Ubuntu-L.ttf')
        page1 = scroll.add_page('pdf/portrait_page.pdf')
        self.assertEqual(page1.size, size)
        page1.add_string('Blah blah blah')
        scroll.render()
        assert os.path.isfile(TEST_PDF)

        page_count = self.pdf.numPages
        assert page_count is 1
        self.assertEqual(self.pdf.getPage(0).get('/Rotate'), 90)


if __name__ == '__main__':
    unittest.main()
