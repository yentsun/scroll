A4_HEIGHT = 595.27

def best_fit(context, width=0, height=0, svg_dim=(0, 0), x=0, y=0,
             frame_top=False, frame_left=False, frame_right=False):
    """
    Scale context to best fit into box
    Credit: http://stackoverflow.com/a/5514344/216042
    Gravity to center
    """
    svg_width = svg_dim[0]
    svg_height = svg_dim[1]

    # formulas
    width_ratio = float(width) / svg_width
    height_ratio = float(height) / svg_height
    scale = min(height_ratio, width_ratio)

    x_margin = 0
    if not frame_left:
        x_margin = (width - svg_width * scale) / 2
    if frame_right:
        x_margin = (width - svg_width * scale)
    y_margin = 0
    if not frame_top:
        y_margin = (height - svg_height * scale) / 2
    context.translate(x + x_margin, y + y_margin)
    context.scale(scale, scale)


def pad_box(canvas_size=(100, 100), padding=(0, 0, 0, 0)):
    """
    Pad the box (cut the size and pad coords) and return new dimensions
    and coords as tuple of tuples
    """
    canvas_width, canvas_height = canvas_size
    padding_top, padding_right, padding_bottom, padding_left = padding
    padded_box_dim = (
        canvas_width - padding_right - padding_left,
        canvas_height - padding_top - padding_bottom
    )
    return padded_box_dim, (padding_left, padding_top)


def from_inkscape_rect(x, ink_y, width, height):
    """Return frame tuple calculated from Inkscape rectangle"""
    y = A4_HEIGHT - ink_y - height
    return x, y, width, height


def from_inkscape_font(ink_size):
    """Return pt size from inkscape font size"""
    font_size = ink_size / 1.25
    return font_size