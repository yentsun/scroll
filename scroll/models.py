import os
import cairo
import rsvg
import uuid
from reportlab.lib.pagesizes import landscape, A4
from reportlab.pdfgen import canvas
from reportlab.lib.colors import HexColor
from pyPdf import PdfFileWriter, PdfFileReader
from scroll import utils
from art3dutils.utilities import create_dirs_in_path

DEFAULT_FONT_ALIAS = 'font'
DEFAULT_FONT_SIZE = 14
TEMP_GRAPHIC_PATH = 'tmp/__content__{id}.pdf'
TEMP_TEXT_PATH = 'tmp/__text__{id}.pdf'


class Tablet(object):
    """class for png output"""
    def __init__(self, output_path, size=(200, 200)):
        width, height = size
        self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
        self.contexts = []
        self.size = size
        self.output_path = output_path

    def add_string(self, string_, font_size=DEFAULT_FONT_SIZE,
                   font_face='sans', coords=(0, 0), hex_color='#000000',
                   new_context=True):
        """Draw a string on cairo context"""
        if new_context:
            context = cairo.Context(self.surface)
            self.contexts.append(context)
        else:
            context = self.contexts[-1]
        context.select_font_face(font_face)
        context.set_font_size(font_size)
        x, y = coords
        context.move_to(x, y)
        context.set_source_rgb(*HexColor(hex_color).rgb())
        context.show_text(string_)
        context.stroke()

    def add_svg(self, svg_path=None, svg_data=None, x=0, y=0, width=None,
                height=None, new_context=True, frame_top=None, frame_left=None):
        """
        Add SVG graphic.
        """
        if new_context:
            context = cairo.Context(self.surface)
            self.contexts.append(context)
        else:
            context = self.contexts[-1]
        svg = None
        if svg_path:
            svg = rsvg.Handle(svg_path)
        if svg_data:
            svg = rsvg.Handle(data=svg_data)
        if not width:
            width = self.size[0]
        if not height:
            height = self.size[1]
        w, h, w1, h1 = svg.get_dimension_data()
        utils.best_fit(context, width=width, height=height, svg_dim=(w, h),
                       x=x, y=y, frame_top=frame_top, frame_left=frame_left)
        svg.render_cairo(context)

    def render(self):
        """Render the PNG"""
        self.surface.flush()
        self.surface.write_to_png(self.output_path)


class Scroll(object):
    """class for PDF output"""

    def __init__(self, output_file_path, size=landscape(A4)):
        self.output_file_path = output_file_path
        self.size = size
        self.pages = []

    def add_page(self, blank_path=None, page=None, page_id=None,
                 blank_page_index=0):
        """Add new page from blank pdf to scroll"""
        if page:
            self.pages.append(page)
            return page
        else:
            new_page = Page(blank_path, size=self.size,
                            blank_page_index=blank_page_index)
            new_page.size = self.size
            new_page.scroll = self
            new_page.id = page_id
            self.pages.append(new_page)
            return new_page

    def register_font(self, alias, path):
        """Register a TTF font for the scroll"""
        from reportlab.pdfbase import pdfmetrics, ttfonts
        pdfmetrics.registerFont(ttfonts.TTFont(alias, path))

    def render(self, compress=True, remove_temp=True):
        """Main rendering process"""
        output = PdfFileWriter()
        for page in self.pages:
            page._finish(remove_temp=remove_temp)
            if compress:
                page.page_object.compressContentStreams()
            output.addPage(page.page_object)
        with open(self.output_file_path, 'w') as f:
            output.write(f)


class Page(object):
    """The page class"""
    def __init__(self, blank_path=None, size=None, blank_page_index=0):
        self.id = uuid.uuid4()
        self.set_blank_path(blank_path, page_index=blank_page_index)
        self.graphics_path = TEMP_GRAPHIC_PATH.format(id=self.id)
        self.text_path = TEMP_TEXT_PATH.format(id=self.id)
        self.size = size
        self.rl_canvas = None
        self.cairo_surface = None  # Cairo PDF surface object
        self.cairo_contexts = []
        create_dirs_in_path(self.text_path)
        create_dirs_in_path(self.graphics_path)
        self.scroll = None

    def set_font(self, alias, size=DEFAULT_FONT_SIZE, path=None):
        """Set font for a page"""
        self.init_rl_canvas()
        if alias not in self.rl_canvas.getAvailableFonts() and path:
            self.scroll.register_font(path=path, alias=alias)
        self.rl_canvas.setFont(alias, size)

    def set_blank_path(self, path, page_index=0):
        self.page_object = PdfFileReader(file(path, 'r')).getPage(page_index)

    def add_paragraph(self, text, x=0, y=0, width=2000, height=0,
                      font_size=DEFAULT_FONT_SIZE,
                      alignment='left', font_face=None,
                      hex_color='#000000', no_p_wrap=False, **style_attribs):
        """Add platypus paragraph"""
        from reportlab.platypus import Paragraph
        from reportlab.lib.styles import ParagraphStyle
        from reportlab.lib.enums import (TA_JUSTIFY, TA_CENTER, TA_LEFT,
                                         TA_RIGHT)

        alignments = {
            'center': TA_CENTER,
            'justify': TA_JUSTIFY,
            'left': TA_LEFT,
            'right': TA_RIGHT
        }
        style_ = ParagraphStyle(name="style_", alignment=alignments[alignment],
                                fontSize=font_size, fontName=font_face,
                                textColor=hex_color, **style_attribs)
        if not no_p_wrap:
            text = u'<p>{text}</p>'.format(text=unicode(text))
        else:
            text = u'{text}'.format(text=unicode(text))
        p = Paragraph(text, style_)
        self.init_rl_canvas()
        p.wrapOn(self.rl_canvas, width, height)
        p.drawOn(self.rl_canvas, x, y)

    def add_string(self, text, x=0, y=0, font_size=None, font_face=None,
                   hex_color=None):
        self.init_rl_canvas()
        if font_face and font_size:
            self.set_font(font_face, font_size)
        if hex_color:
            self.rl_canvas.setFillColor(HexColor(hex_color))
        self.rl_canvas.drawString(x, y, text)

    def add_SVG(self, path=None, data=None, x=0, y=0, scale=None,
                new_context=True, width=None, height=None, frame_top=False,
                frame_left=False, frame_right=False, frame=None):
        """Add SVG graphic with cairo and rsvg"""
        if frame:
            x, y, width, height = frame
        if not self.cairo_surface:
            self.cairo_surface = cairo.PDFSurface(self.graphics_path,
                                                  self.size[0], self.size[1])
        svg = None
        if path:
            svg = rsvg.Handle(path)
        if data:
            svg = rsvg.Handle(data=data)
        if new_context:
            context = cairo.Context(self.cairo_surface)
            self.cairo_contexts.append(context)
            if scale:
                context.translate(x, y)
                context.scale(scale, scale)
            else:
                if not width:
                    width = self.size[0]
                if not height:
                    height = self.size[1]
                w, h, w1, h1 = svg.get_dimension_data()
                utils.best_fit(context, x=x, y=y, width=width, height=height,
                               svg_dim=(w, h), frame_top=frame_top,
                               frame_left=frame_left, frame_right=frame_right)
        else:
            context = self.cairo_contexts[-1]

        svg.render_cairo(context)

    def add_PNG(self, path, scale=1):
        """Add PNG image with cairo"""

    #TODO set this as working context (through translate and scale)
    def add_frame(self, x, y, width, height, line_width=1):
        """Add rectangle with cairo"""
        if not self.cairo_surface:
            self.cairo_surface = cairo.PDFSurface(self.graphics_path,
                                                  self.size[0], self.size[1])
        context = cairo.Context(self.cairo_surface)
        context.set_line_width(line_width)
        context.set_source_rgb(255, 0, 0)
        context.rectangle(x, y, width, height)
        context.stroke()
        self.cairo_contexts.append(context)

    def init_rl_canvas(self):
        """"""
        if not self.rl_canvas:
            self.rl_canvas = canvas.Canvas(self.text_path, pagesize=self.size)

    def draw_rectangle(self, x, y, width, height, rgb=(255, 255, 255)):
        """Draw a rectangle on a cairo context"""
        if not self.cairo_surface:
            self.cairo_surface = cairo.PDFSurface(self.graphics_path,
                                                  self.size[0], self.size[1])
        context = cairo.Context(self.cairo_surface)
        context.rectangle(x, y, width, height)
        context.set_source_rgb(*rgb)
        context.fill()
        self.cairo_contexts.append(context)

    def _finish(self, remove_temp=True):
        """Finalize the page (merge temp pdf files)"""
        if self.rl_canvas:
            self.rl_canvas.save()
            text_page = PdfFileReader(file(self.text_path, 'r')).getPage(0)
            self.page_object.mergePage(text_page)
            if remove_temp:
                os.remove(self.text_path)
            self.rl_canvas = None

        if self.cairo_surface:
            for context in self.cairo_contexts:
                context.show_page()
            self.cairo_surface.finish()
            graphics_page = PdfFileReader(file(self.graphics_path, 'r')).getPage(0)
            self.page_object.mergePage(graphics_page)
            if remove_temp:
                os.remove(self.graphics_path)
            self.cairo_surface = None