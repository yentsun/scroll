Welcome to Scroll's documentation!
==================================

Scroll is a simple library for mass PDF brochure/booklet generation, inspired
by TCPDF_. It uses SVG as vector graphic format and utilizes

* pycairo_ and pyrsvg_ for svg import
* reportlab_ for text import
* pypdf_ and for merging pages into the final document

.. _pycairo: http://www.cairographics.org/pycairo/
.. _pypdf: http://pybrary.net/pyPdf/
.. _reportlab: http://www.reportlab.com/
.. _pyrsvg: http://cairographics.org/pyrsvg/
.. _TCPDF: http://www.tcpdf.org/

Example
=======

A simple one-page booklet generation code:

.. code-block:: python

        from scroll import Scroll

        scroll = Scroll('output_file_path.pdf')

        #assume we have a blank pdf (maybe with a logo and some stationery)
        page1 = scroll.add_page('blank.pdf')

        #add graphics
        page1.add_SVG(path='floor.svg', coords=(290, 0), scale=0.85)
        #the following svg will be drawn with the same coordinates and scale as
        #previous
        page1.add_SVG(path='outline.svg', new_context=False)
        page1.add_SVG(path='minimap.svg', coords=(40, 290), scale=0.89)

        #add text
        page1.add_text_block(font_path='Ubuntu.ttf',
                             coords=(40, 453),
                             font_size=16,
                             lines=(
                                u'Building 1',
                                u'Section 2',
                                u'Floor 1'
                             ))
        page1.add_text_block(coords=(40, 345),
                             font_size=12,
                             lines=(
                                u'Room count: 3',
                                u'Square: 64.56 m²'
                             ))
        scroll.render()

