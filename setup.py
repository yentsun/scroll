from setuptools import setup, find_packages

setup(name='scroll',
    version='0.1.2',
    author='Max Korinets',
    author_email='mkorinets@gmail.com',
    license='MIT',
    description='A simple library for pdf generation, inspired by TCPDF',
    install_requires=[
        'reportlab',
        'pyPdf',
        'art3dutils'
        ],
    include_package_data=True,
    packages=find_packages())